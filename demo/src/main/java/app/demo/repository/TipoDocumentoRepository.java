package app.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.demo.modelo.dao.TipoDocumento;

@Repository
public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Integer> {
	
	@Query("SELECT t FROM TipoDocumento t WHERE t.codigo = :codigo")
	public TipoDocumento getTipoDocumentoxCodigo(@Param("codigo") String codigo);
}
