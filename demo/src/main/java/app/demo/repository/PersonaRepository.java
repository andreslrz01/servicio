package app.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.demo.modelo.dao.Persona;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long>{
	
	@Query("SELECT u FROM Persona u WHERE u.numeroDocumento = :identificacion")
	public Persona consultarPersonaxIdentificacion(@Param("identificacion") String identificacion);
}
