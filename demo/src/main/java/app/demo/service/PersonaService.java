package app.demo.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.demo.controller.Facade;
import app.demo.modelo.dao.Persona;

@RestController
@RequestMapping("/persona")
public class PersonaService {
	
	@Autowired
	private Facade<Persona> facade;

	@PostMapping("/get")
	public ResponseEntity<Persona> getPersona(HttpServletResponse response, @RequestBody Persona persona) {
		Persona pdto = facade.buscar(persona);
		if(pdto != null) {
			return ResponseEntity.ok(pdto);
		} else {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}
		return null;
	}
	
	@PutMapping("/new")
	public void crearPersona(HttpServletResponse response, @RequestBody Persona persona) {
		if(persona == null || facade.buscar(persona) != null) {
			response.setStatus(HttpStatus.CONFLICT.value());
		} 
		facade.crear(persona);
	}
}
