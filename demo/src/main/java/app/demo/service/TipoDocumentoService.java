package app.demo.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.demo.controller.Facade;
import app.demo.modelo.dao.TipoDocumento;

@RestController
@RequestMapping("/tipodocumento")
public class TipoDocumentoService {

	@Autowired
	private Facade<TipoDocumento> facade;
	
	@PutMapping("/new")
	public void crearTipoDocumento(HttpServletResponse response, @RequestBody TipoDocumento tipoDoc) {
		facade.crear(tipoDoc);
		response.setStatus(HttpStatus.CREATED.value());
	}
	
	@GetMapping("/getAll")
	public List<TipoDocumento> getAll() {
		return facade.listar();
	}
	
}
