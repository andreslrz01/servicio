package app.demo.modelo.dao;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "TB_DOCUMENTOS")
@JsonIgnoreProperties(value = { "personas", "fechaCreacion", "fechaModificacion"}, allowGetters = true)
public class TipoDocumento extends AuditModel {

	private static final long serialVersionUID = -4352753806288076950L;

	@Id
	@GeneratedValue(generator = "TIPO_DOCUMENTO_GENERATOR")
    @SequenceGenerator(
            name = "TIPO_DOCUMENTO_GENERATOR",
            sequenceName = "TIPO_DOCUMENTO_SEQUENCE",
            initialValue = 1
    )
	private int id;
	
	@Column(name = "CODIGO", nullable = false, unique = true, length = 3)
	private String codigo;
	
	@Column(name = "DESCRIPCION", nullable = false, length = 200)
	private String descripcion;
	
	@OneToMany(mappedBy = "tipoDocumento")
	private List<Persona> personas;

	public TipoDocumento() {
		super();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	

}
