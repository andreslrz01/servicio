package app.demo.modelo.dao;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_PERSONAS")
public class Persona extends AuditModel {

	private static final long serialVersionUID = 3463813175977001985L;

	@Id
	@GeneratedValue(generator = "PERSONA_GENERATOR")
	@SequenceGenerator(name = "PERSONA_GENERATOR", sequenceName = "PERSONA_SEQUENCE", initialValue = 1)
	private long id;

	@Column(name = "NUMERO_DOCUMENTO", nullable = false, length = 20, unique = true)
	private String numeroDocumento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TIPO_IDENTIFICACION_ID")
	private TipoDocumento tipoDocumento;
	
	@Column(name = "NOMBRE", nullable = false, length = 50)
	private String nombre;
	
	@Column(name = "APELLIDOS", nullable = false, length = 50)
	private String apellidos;
	
	@Column(name = "FECHA_NACIMIENTO", nullable = false, length = 50)
	private Date fechaNacimiento;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

}
