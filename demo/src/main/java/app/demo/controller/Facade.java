package app.demo.controller;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface Facade<T> {
	
	public T buscar(T t);
	public boolean crear(T t);
	public boolean actualizar(T t);
	public boolean borrar(T t);
	public List<T> listar();
	
}
