package app.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.demo.modelo.dao.TipoDocumento;
import app.demo.repository.TipoDocumentoRepository;

@Service
public class TipoDocumentoController implements Facade<TipoDocumento> {

	@Autowired
	private TipoDocumentoRepository repo;
	
	@Override
	public TipoDocumento buscar(TipoDocumento t) {
		return repo.getTipoDocumentoxCodigo(t.getCodigo());
	}

	@Override
	public boolean crear(TipoDocumento t) {
		Date fechaActual = new Date();
		t.setFechaCreacion(fechaActual);
		t.setFechaModificacion(fechaActual);
		TipoDocumento entity = repo.saveAndFlush(t);
		return entity != null;
	}

	@Override
	public boolean actualizar(TipoDocumento t) {
		Date fechaActual = new Date();
		t.setFechaModificacion(fechaActual);
		TipoDocumento entity = repo.saveAndFlush(t);
		return entity != null;
	}

	@Override
	public boolean borrar(TipoDocumento t) {
		repo.delete(t);
		return true;
	}

	@Override
	public List<TipoDocumento> listar() {
		return repo.findAll();
	}

}
