package app.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.demo.modelo.dao.Persona;
import app.demo.repository.PersonaRepository;

@Service
public class PersonaController implements Facade<Persona> {

	@Autowired
	private PersonaRepository repo;

	@Override
	public Persona buscar(Persona persona) {
		return repo.consultarPersonaxIdentificacion(persona.getNumeroDocumento());
	}

	@Override
	public boolean crear(Persona persona) {
		Date fechaActual = new Date();
		persona.setFechaCreacion(fechaActual);
		persona.setFechaModificacion(fechaActual);
		Persona result = repo.saveAndFlush(persona);
		return result != null;
	}
	

	@Override
	public boolean actualizar(Persona persona) {
		Date fechaActual = new Date();
		persona.setFechaModificacion(fechaActual);
		Persona result = repo.saveAndFlush(persona);
		return result != null;
	}

	@Override
	public boolean borrar(Persona persona) {
		repo.delete(persona);
		return false;
	}

	@Override
	public List<Persona> listar() {
		return repo.findAll();
		
	}

}
